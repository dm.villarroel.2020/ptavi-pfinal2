#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import socket
import socketserver
import time
import threading
import simplertp


IP = '127.0.0.1'
PORT = 34543


def get_time(): #Función que retorna la fecha y hora actual en un formato específico utilizado para registrar eventos
    actual_time = time.localtime()
    year = str(actual_time.tm_year).zfill(2)
    month = str(actual_time.tm_mon).zfill(2)
    day = str(actual_time.tm_mday).zfill(2)
    hour = str(actual_time.tm_hour).zfill(2)
    minute = str(actual_time.tm_min).zfill(2)
    second = str(actual_time.tm_sec).zfill(2)

    return f'{year}{month}{day}{hour}{minute}{second}'


def get_arguments(): #Función que obtiene los argumentos pasados al programa desde la línea de comandos.
    # Si los argumentos no se proporcionan correctamente, el programa muestra un mensaje de error y se cierra.

    # Comprobar formato argumentos terminal.

    # $ python3 client.py 127.0.0.1:6001 sip:cliente1@clientes.net sip:servidor1@songs.net cancion.mp3
    # $ python3 client.py <IPServidorSIP>:<puertoServidorSIP> <dirCliente> <dirServidorRTP> <fichero>
    # Usage: python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP> <file>

    try:
        ip_servsip = sys.argv[1].split(':')[0]
        port_servsip = int(sys.argv[1].split(':')[1])
        sip_client = sys.argv[2]
        sip_servrtp = sys.argv[3]
        file = sys.argv[4]

    except:
        sys.exit('Usage: python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP> <file>')

    return ip_servsip, port_servsip, sip_client, sip_servrtp, file


def main(): #Envía mensajes SIP al servidor y gestiona las respuestas recibidas.

    ip_servsip, port_servsip, sip_client, sip_servrtp, file = get_arguments()

    # Comunicación con socket UDP.
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            print(f'{get_time()} Starting...')  # Histórico

            #REGISTER inicial
            message = f'REGISTER {sip_client} SIP/2.0\r\n\r\n'
            my_socket.sendto(message.encode('utf-8'), (ip_servsip, port_servsip))
            print(f'{get_time()} SIP to {ip_servsip}:{port_servsip}: {message}')  # Histórico

            #Manejamos respuestas del servidor
            data, address = my_socket.recvfrom(1024)
            print(f'{get_time()} SIP from {address[0]}:{address[1]}: {data.decode("utf-8")}')  # Histórico

            # INVITE
            protocol = f'INVITE {sip_servrtp} SIP/2.0\r\nFrom: <{sip_client}>'
            content_type = 'Content-type: application/sdp'
            session_name = sip_servrtp.split("@")[0].split(":")[1] # cliente1
            sdp_elements = f'v=0\r\no={sip_client} {IP}\r\ns={session_name}\r\nt=0\r\nm=audio {PORT} RTP'
            message = f'{protocol}\r\n{content_type}\r\n\r\n{sdp_elements}\r\n'
            my_socket.sendto(message.encode('utf-8'), (ip_servsip, port_servsip))
            print(f'{get_time()} SIP to {ip_servsip}:{port_servsip}: {message}')  # Histórico

            # Manejamos respuestas del servidor
            data, address = my_socket.recvfrom(1024)
            print(f'{get_time()} SIP from {address[0]}:{address[1]}: {data.decode("utf-8")}')  # Histórico


            # Enviamos respuesta si el mensaje del servidor es el siguiente.
            if data.decode('utf-8').split('\r\n')[0] == 'SIP/2.0 302 Moved Temporarily':
                message = f'ACK {sip_servrtp} SIP/2.0\r\nFrom: <{sip_client}>\r\n\r\n'
                my_socket.sendto(message.encode('utf-8'), (ip_servsip, port_servsip))
                print(f'{get_time()} SIP to {ip_servsip}:{port_servsip}: {message}')  # Histórico

                # Bloque de envío al servidorrtp
                wanted_address = data.decode('utf-8').split('\r\n')[1].split()[1]
                wanted_address_ip = wanted_address.split('@')[1].split(':')[0]
                wanted_address_port = int(wanted_address.split('@')[1].split(':')[1])

                # Mensaje para servertp
                protocol = f'INVITE {wanted_address} SIP/2.0'
                content_type = 'Content-type: application/sdp'
                session_name = wanted_address.split("@")[0].split(":")[1]
                sdp_elements = f'v=0\r\no={sip_client} {IP}\r\ns={session_name}\r\nt=0\r\nm=audio {PORT} RTP'
                message = f'{protocol}\r\n{content_type}\r\n\r\n{sdp_elements}\r\n'
                my_socket.sendto(message.encode('utf-8'), (wanted_address_ip, wanted_address_port))
                print(f'{get_time()} SIP to {wanted_address_ip}:{wanted_address_port}: {message}')  # Histórico

                # Gestión de mensaje recibido de serverrtp
                data, address = my_socket.recvfrom(1024)
                print(f'{get_time()} SIP from {address[0]}:{address[1]}: {data.decode("utf-8")}')  # Histórico

                # Comprobamos mensaje recibido de serverrtp
                if data.decode('utf-8').split('\r\n')[0] == 'SIP/2.0 200 OK':

                    # Mensaje ACK a ServerRTP
                    message = f'ACK {sip_servrtp.split("@")[0]}@{address[0]}:{address[1]} SIP/2.0\r\nFrom: <{sip_client}>\r\n\r\n'
                    my_socket.sendto(message.encode('utf-8'), (wanted_address_ip, wanted_address_port))
                    print(f'{get_time()} SIP to {wanted_address_ip}:{wanted_address_port}: {message}')  # Histórico

                    # IP y puerto para enviar datos RTP al ServerRTP
                    received = data.decode('utf-8')
                    serverRTP_IP = received.split('o=')[1].split()[1]  # '127.0.0.1'
                    serverRTP_PORT = int(received.split('audio ')[1].split()[0])  # 20354

                    # Envío de datos RTP.
                    sender = simplertp.RTPSender(ip=serverRTP_IP, port=serverRTP_PORT, file=file, printout=True)
                    sender.send_threaded()
                    time.sleep(5)
                    print("Finalizando el thread de envío.")
                    sender.finish()


                    # Enviamos BYE
                    message = f'BYE {sip_servrtp.split("@")[0]}@{address[0]}:{address[1]} SIP/2.0\r\n\r\n'
                    my_socket.sendto(message.encode('utf-8'), (wanted_address_ip, wanted_address_port))
                    print(f'{get_time()} SIP to {wanted_address_ip}:{wanted_address_port}: {message}')  # Histórico

                    # Esperando OK del serverRTP.
                    data, address = my_socket.recvfrom(1024)
                    print(f'{get_time()} SIP from {address[0]}:{address[1]}: {data.decode("utf-8")}')  # Histórico
                    # Recibimos OK y cierra cliente.
                    if data.decode('utf-8') == 'SIP/2.0 200 OK\r\n\r\n':
                        my_socket.close()

    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == '__main__':
    main()
