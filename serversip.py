<<<<<<< HEAD
#!/usr/bin/python3
# -*- coding: utf-8 -*-
import datetime
import sys
import socketserver
import json
import time


def get_time():
    actual_time = time.localtime()
    year = str(actual_time.tm_year).zfill(2)
    month = str(actual_time.tm_mon).zfill(2)
    day = str(actual_time.tm_mday).zfill(2)
    hour = str(actual_time.tm_hour).zfill(2)
    minute = str(actual_time.tm_min).zfill(2)
    second = str(actual_time.tm_sec).zfill(2)

    return f'{year}{month}{day}{hour}{minute}{second}'


def get_arguments():
    # Comprobar formato argumentos terminal.
    # python3 server.py 6001
    try:
        port = int(sys.argv[1])

    except:
        sys.exit('Usage: python3 serversip.py <port>')

    return port


def there_is_dicc():
    users_dict = {}
    try:
        fich = open('registrar.json', 'r')
        data = json.loads(fich.read())
        fich.close()
        for user in data:
            for key, value in user.items():
                users_dict[key] = value

    except:
        pass

    return users_dict


class EchoHandler(socketserver.BaseRequestHandler):
    # Diccionario de usuarios, comprobando si existe archivo .json
    usersDict = there_is_dicc()

    def process_register(self, sip, real):
        # Registrar usuarios
        self.usersDict[sip] = real  # Actualizo diccionario

        # Actualizo archivo registrar.json
        fich = open('registrar.json', 'w')
        fich.write(str(json.dumps([self.usersDict], indent=1)))
        fich.close()

    def search_address(self, sip_address):
        # Busca direcciones en el diccionario
        try:
            return self.usersDict[sip_address]
        except:
            return 'User Not Found'

    def handle(self):
        data = self.request[0]
        sock = self.request[1]
        received = data.decode('utf-8')
        petition = received.split()[0]

        # Histórico
        print(f'{get_time()} SIP from {self.client_address[0]}:{self.client_address[1]}: {data.decode("utf-8")}')

        # Comprobar el formato.
        if type(received) is not str:
            response = 'SIP/2.0 400 Bad Request'
            sock.sendto(response.encode('utf-8'), self.client_address)

        # Caso método INVITE.
        if petition == 'INVITE':

            # Comprobar que la dirección que pide el invite está en los dicconarios
            try:
                user = self.search_address(received.split('From: ')[1].split('\r\n')[0])
            except:
                response = f'SIP/2.0 404 User Not Found\r\n\r\n'
                sock.sendto(response.encode('utf-8'), self.client_address)
                print(f'{get_time()} SIP to {self.client_address[0]}:{self.client_address[1]}: {response}')  # Histórico

            else:
                # Para contetsar al cliente con la dirección del servidorRTP
                user = self.search_address(received.split()[1])
                if user == 'User Not Found':
                    response = f'SIP/2.0 404 User Not Found\r\n\r\n'
                    sock.sendto(response.encode('utf-8'), self.client_address)
                    print(f'{get_time()} SIP to {self.client_address[0]}:{self.client_address[1]}: {response}')  # Histórico
                else:
                    response = f'SIP/2.0 302 Moved Temporarily\r\nContact: {user}\r\n\r\n'
                    sock.sendto(response.encode('utf-8'), self.client_address)
                    print(f'{get_time()} SIP to {self.client_address[0]}:{self.client_address[1]}: {response}')  # Histórico

        # Caso REGISTER
        elif petition == 'REGISTER':
            sip = received.split()[1]
            real = f'{sip.split("@")[0]}@{self.client_address[0]}:{self.client_address[1]}'
            self.process_register(sip, real)
            response = f'SIP/2.0 200 OK\r\nFrom: <127.0.0.1:6001>\r\n\r\n'
            sock.sendto(response.encode('utf-8'), self.client_address)
            print(f'{get_time()} SIP to {self.client_address[0]}:{self.client_address[1]}: {response}')  # Histórico

        elif petition == 'ACK':
            pass


def main():
    port = get_arguments()

    try:
        serv = socketserver.UDPServer(('', port), EchoHandler)
        print(f'{get_time()} Starting...')  # Histórico
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == '__main__':
    main()
=======
#!/usr/bin/python3
# -*- coding: utf-8 -*-
import datetime
import sys
import socketserver
import json
import time


def get_time():
    actual_time = time.localtime()
    year = str(actual_time.tm_year).zfill(2)
    month = str(actual_time.tm_mon).zfill(2)
    day = str(actual_time.tm_mday).zfill(2)
    hour = str(actual_time.tm_hour).zfill(2)
    minute = str(actual_time.tm_min).zfill(2)
    second = str(actual_time.tm_sec).zfill(2)

    return f'{year}{month}{day}{hour}{minute}{second}'


def get_arguments():
    # Comprobar formato argumentos terminal.
    # python3 server.py 6001
    try:
        port = int(sys.argv[1])

    except:
        sys.exit('Usage: python3 serversip.py <port>')

    return port


def there_is_dicc():
    users_dict = {}
    try:
        fich = open('registrar.json', 'r')
        data = json.loads(fich.read())
        fich.close()
        for user in data:
            for key, value in user.items():
                users_dict[key] = value

    except:
        pass

    return users_dict


class EchoHandler(socketserver.BaseRequestHandler):
    # Diccionario de usuarios, comprobando si existe archivo .json
    usersDict = there_is_dicc()

    def process_register(self, sip, real):
        # Registrar usuarios
        self.usersDict[sip] = real  # Actualizo diccionario

        # Actualizo archivo registrar.json
        fich = open('registrar.json', 'w')
        fich.write(str(json.dumps([self.usersDict], indent=1)))
        fich.close()

    def search_address(self, sip_address):
        # Busca direcciones en el diccionario
        try:
            return self.usersDict[sip_address]
        except:
            return 'User Not Found'

    def handle(self):
        data = self.request[0]
        sock = self.request[1]
        received = data.decode('utf-8')
        petition = received.split()[0]

        # Histórico
        print(f'{get_time()} SIP from {self.client_address[0]}:{self.client_address[1]}: {data.decode("utf-8")}')

        # Comprobar el formato.
        if type(received) is not str:
            response = 'SIP/2.0 400 Bad Request'
            sock.sendto(response.encode('utf-8'), self.client_address)

        # Caso método INVITE.
        if petition == 'INVITE':

            # Comprobar que la dirección que pide el invite está en los dicconarios
            try:
                user = self.search_address(received.split('From: ')[1].split('\r\n')[0])
            except:
                response = f'SIP/2.0 404 User Not Found\r\n\r\n'
                sock.sendto(response.encode('utf-8'), self.client_address)
                print(f'{get_time()} SIP to {self.client_address[0]}:{self.client_address[1]}: {response}')  # Histórico

            else:
                # Para contetsar al cliente con la dirección del servidorRTP
                user = self.search_address(received.split()[1])
                if user == 'User Not Found':
                    response = f'SIP/2.0 404 User Not Found\r\n\r\n'
                    sock.sendto(response.encode('utf-8'), self.client_address)
                    print(f'{get_time()} SIP to {self.client_address[0]}:{self.client_address[1]}: {response}')  # Histórico
                else:
                    response = f'SIP/2.0 302 Moved Temporarily\r\nContact: {user}\r\n\r\n'
                    sock.sendto(response.encode('utf-8'), self.client_address)
                    print(f'{get_time()} SIP to {self.client_address[0]}:{self.client_address[1]}: {response}')  # Histórico

        # Caso REGISTER
        elif petition == 'REGISTER':
            sip = received.split()[1]
            real = f'{sip.split("@")[0]}@{self.client_address[0]}:{self.client_address[1]}'
            self.process_register(sip, real)
            response = f'SIP/2.0 200 OK\r\nFrom: <127.0.0.1:6001>\r\n\r\n'
            sock.sendto(response.encode('utf-8'), self.client_address)
            print(f'{get_time()} SIP to {self.client_address[0]}:{self.client_address[1]}: {response}')  # Histórico

        elif petition == 'ACK':
            pass


def main():
    port = get_arguments()

    try:
        serv = socketserver.UDPServer(('', port), EchoHandler)
        print(f'{get_time()} Starting...')  # Histórico
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == '__main__':
    main()
>>>>>>> origin/master
