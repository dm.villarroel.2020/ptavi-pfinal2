<<<<<<< HEAD
#!/usr/bin/python3
# -*- coding: utf-8 -*-

import threading
import random
import sys
import socket
import socketserver
import time
import simplertp

IP = '127.0.0.1'
PORT = 20354  # Puerto de escucha RTP


class RTPHandler(socketserver.BaseRequestHandler):
    """Clase para manejar los paquetes RTP que nos lleguen.

    Atención: Heredamos de BaseRequestHandler porque UDPRequestHandler
    siempre termina enviando una respuesta al cliente, algo que aquí no
    queremos hacer. Al usar BaseRequestHandler no podemos usar self.rfile
    (porque esta clase no lo tiene), así que leemos el mensaje directamente
    de self.request (que es una lista, cuyo primer elemento es el mensaje
    recibido)"""

    def handle(self):
        msg = self.request[0]
        # sólo nos interesa lo que hay a partir del byte 54 del paquete UDP
        # que es donde está la payload del paquete RTP qeu va dentro de él
        payload = msg[12:]
        # Escribimos esta payload en el fichero de salida
        self.output.write(payload)
        print("Received")

    @classmethod
    def open_output(cls, filename):
        """Abrir el fichero donde se va a escribir lo recibido.

        Lo abrimos en modo escritura (w) y binario (b)
        Es un método de la clase para que podamos invocarlo sobre la clase
        antes de empezar a recibir paquetes."""

        cls.output = open(filename, 'wb')

    @classmethod
    def close_output(cls):
        """Cerrar el fichero donde se va a escribir lo recibido.

        Es un método de la clase para que podamos invocarlo sobre la clase
        después de terminar de recibir paquetes."""
        cls.output.close()


def get_time():
    actual_time = time.localtime()
    year = str(actual_time.tm_year).zfill(2)
    month = str(actual_time.tm_mon).zfill(2)
    day = str(actual_time.tm_mday).zfill(2)
    hour = str(actual_time.tm_hour).zfill(2)
    minute = str(actual_time.tm_min).zfill(2)
    second = str(actual_time.tm_sec).zfill(2)

    return f'{year}{month}{day}{hour}{minute}{second}'


def get_arguments():
    # Comprobar formato argumentos terminal.
    # python3 serverrtp.py <IPServidorSIP>:<puertoServidorSIP> <servicio>
    # python3 serverrtp.py 127.0.0.1:6001 servidor1

    try:
        ip_servsip = sys.argv[1].split(':')[0]
        port_servsip = int(sys.argv[1].split(':')[1])
        servicio = sys.argv[2]

    except:
        sys.exit('Usage: python3 serverrtp.py <IPServerSIP>:<portServerSIP> <service>')

    return ip_servsip, port_servsip, servicio


def main():
    ip_servsip, port_servsip, servicio = get_arguments()
    file = 'recibido.mp3'

    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:

            try:
                # Envía REGISTER a serverSIP al iniciar el serverrtp
                message = f'REGISTER sip:{servicio}@songs.net SIP/2.0\r\n\r\n'
                my_socket.sendto(message.encode('utf-8'), (ip_servsip, port_servsip))
                print(f'{get_time()} SIP to {ip_servsip}:{port_servsip}: {message}')  # Histórico
            except:
                sys.exit('Error registrando serverrtp en serversip')
            
            else:
                while True:
                    data, client_address = my_socket.recvfrom(1024)  # Datos y dirección del emisor
                    received = data.decode('utf-8')  # Datos recibidos decodificados
                    petition = received.split()[0]  # Petición

                    # Histórico
                    print(f'{get_time()} SIP from {client_address[0]}:{client_address[1]}: {data.decode("utf-8")}')

                    if petition == 'INVITE':
                        # Envía respuesta
                        protocol = f'SIP/2.0 200 OK\r\nFrom: <sip:{servicio}@songs.net>'
                        content_type = 'Content-type: application/sdp'
                        client_sip_dir = received.split('o=')[1].split(' ')[0]
                        session_name = received.split('s=')[1].split()[0] # obtiene lo que hay en s= -> cliente1
                        sdp_elements = f'v=0\r\no={client_sip_dir} {client_address[0]}\r\ns={session_name}\r\nt=0\r\nm=audio {PORT} RTP'

                        response = f'{protocol}\r\n{content_type}\r\n\r\n{sdp_elements}\r\n'
                        my_socket.sendto(response.encode('utf-8'), client_address)
                        print(f'{get_time()} SIP to {client_address[0]}:{client_address[1]}: {response}')  # Histórico

                    elif petition == 'ACK':

                        # Se queda escuchando para recibir los paquetes RTP
                        # Abrimos un fichero para escribir los datos que se reciban
                        RTPHandler.open_output(file)
                        with socketserver.UDPServer((IP, PORT), RTPHandler) as serv:
                            print("Listening...")
                            # El bucle de recepción (serv_forever) va en un hilo aparte,
                            # para que se continue ejecutando este programa principal,
                            # y podamos interrumpir ese bucle más adelante
                            threading.Thread(target=serv.serve_forever).start()
                            # Paramos un rato. Igualmente, podríamos esperar a recibir BYE,

                            data, client_address = my_socket.recvfrom(1024)
                            received = data.decode('utf-8')

                            print(f'{get_time()} SIP from {client_address[0]}:{client_address[1]}: {received}')  # Histórico

                            if received.split(' ')[0] == 'BYE':
                                print("BYE received, shutting down receiver loop.")
                                # Paramos el bucle de recepción, con lo que terminará el thread,
                                # y dejamos de recibir paquetes
                                serv.shutdown()
                                # Cerramos el fichero donde estamos escribiendo los datos recibidos
                                RTPHandler.close_output()

                                # Se envia la respuesta OK al cliente
                                response = f'SIP/2.0 200 OK\r\nFrom: <sip:{servicio}@songs.net>\r\n\r\n'
                                my_socket.sendto(response.encode('utf-8'), client_address)
                                print(f'{get_time()} SIP to {client_address[0]}:{client_address[1]}: {response}')

    except ConnectionRefusedError:
        print("Error conectando al servidor")


if __name__ == '__main__':
    main()
=======
#!/usr/bin/python3
# -*- coding: utf-8 -*-

import threading
import random
import sys
import socket
import socketserver
import time
import simplertp

IP = '127.0.0.1'
PORT = 20354  # Puerto de escucha RTP


class RTPHandler(socketserver.BaseRequestHandler):
    """Clase para manejar los paquetes RTP que nos lleguen.

    Atención: Heredamos de BaseRequestHandler porque UDPRequestHandler
    siempre termina enviando una respuesta al cliente, algo que aquí no
    queremos hacer. Al usar BaseRequestHandler no podemos usar self.rfile
    (porque esta clase no lo tiene), así que leemos el mensaje directamente
    de self.request (que es una lista, cuyo primer elemento es el mensaje
    recibido)"""

    def handle(self):
        msg = self.request[0]
        # sólo nos interesa lo que hay a partir del byte 54 del paquete UDP
        # que es donde está la payload del paquete RTP qeu va dentro de él
        payload = msg[12:]
        # Escribimos esta payload en el fichero de salida
        self.output.write(payload)
        print("Received")

    @classmethod
    def open_output(cls, filename):
        """Abrir el fichero donde se va a escribir lo recibido.

        Lo abrimos en modo escritura (w) y binario (b)
        Es un método de la clase para que podamos invocarlo sobre la clase
        antes de empezar a recibir paquetes."""

        cls.output = open(filename, 'wb')

    @classmethod
    def close_output(cls):
        """Cerrar el fichero donde se va a escribir lo recibido.

        Es un método de la clase para que podamos invocarlo sobre la clase
        después de terminar de recibir paquetes."""
        cls.output.close()


def get_time():
    actual_time = time.localtime()
    year = str(actual_time.tm_year).zfill(2)
    month = str(actual_time.tm_mon).zfill(2)
    day = str(actual_time.tm_mday).zfill(2)
    hour = str(actual_time.tm_hour).zfill(2)
    minute = str(actual_time.tm_min).zfill(2)
    second = str(actual_time.tm_sec).zfill(2)

    return f'{year}{month}{day}{hour}{minute}{second}'


def get_arguments():
    # Comprobar formato argumentos terminal.
    # python3 serverrtp.py <IPServidorSIP>:<puertoServidorSIP> <servicio>
    # python3 serverrtp.py 127.0.0.1:6001 servidor1

    try:
        ip_servsip = sys.argv[1].split(':')[0]
        port_servsip = int(sys.argv[1].split(':')[1])
        servicio = sys.argv[2]

    except:
        sys.exit('Usage: python3 serverrtp.py <IPServerSIP>:<portServerSIP> <service>')

    return ip_servsip, port_servsip, servicio


def main():
    ip_servsip, port_servsip, servicio = get_arguments()
    file = 'recibido.mp3'

    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:

            try:
                # Envía REGISTER a serverSIP al iniciar el serverrtp
                message = f'REGISTER sip:{servicio}@songs.net SIP/2.0\r\n\r\n'
                my_socket.sendto(message.encode('utf-8'), (ip_servsip, port_servsip))
                print(f'{get_time()} SIP to {ip_servsip}:{port_servsip}: {message}')  # Histórico
            except:
                sys.exit('Error registrando serverrtp en serversip')
            
            else:
                while True:
                    data, client_address = my_socket.recvfrom(1024)  # Datos y dirección del emisor
                    received = data.decode('utf-8')  # Datos recibidos decodificados
                    petition = received.split()[0]  # Petición

                    # Histórico
                    print(f'{get_time()} SIP from {client_address[0]}:{client_address[1]}: {data.decode("utf-8")}')

                    if petition == 'INVITE':
                        # Envía respuesta
                        protocol = f'SIP/2.0 200 OK\r\nFrom: <sip:{servicio}@songs.net>'
                        content_type = 'Content-type: application/sdp'
                        client_sip_dir = received.split('o=')[1].split(' ')[0]
                        session_name = received.split('s=')[1].split()[0] # obtiene lo que hay en s= -> cliente1
                        sdp_elements = f'v=0\r\no={client_sip_dir} {client_address[0]}\r\ns={session_name}\r\nt=0\r\nm=audio {PORT} RTP'

                        response = f'{protocol}\r\n{content_type}\r\n\r\n{sdp_elements}\r\n'
                        my_socket.sendto(response.encode('utf-8'), client_address)
                        print(f'{get_time()} SIP to {client_address[0]}:{client_address[1]}: {response}')  # Histórico

                    elif petition == 'ACK':

                        # Se queda escuchando para recibir los paquetes RTP
                        # Abrimos un fichero para escribir los datos que se reciban
                        RTPHandler.open_output(file)
                        with socketserver.UDPServer((IP, PORT), RTPHandler) as serv:
                            print("Listening...")
                            # El bucle de recepción (serv_forever) va en un hilo aparte,
                            # para que se continue ejecutando este programa principal,
                            # y podamos interrumpir ese bucle más adelante
                            threading.Thread(target=serv.serve_forever).start()
                            # Paramos un rato. Igualmente, podríamos esperar a recibir BYE,

                            data, client_address = my_socket.recvfrom(1024)
                            received = data.decode('utf-8')

                            print(f'{get_time()} SIP from {client_address[0]}:{client_address[1]}: {received}')  # Histórico

                            if received.split(' ')[0] == 'BYE':
                                print("BYE received, shutting down receiver loop.")
                                # Paramos el bucle de recepción, con lo que terminará el thread,
                                # y dejamos de recibir paquetes
                                serv.shutdown()
                                # Cerramos el fichero donde estamos escribiendo los datos recibidos
                                RTPHandler.close_output()

                                # Se envia la respuesta OK al cliente
                                response = f'SIP/2.0 200 OK\r\nFrom: <sip:{servicio}@songs.net>\r\n\r\n'
                                my_socket.sendto(response.encode('utf-8'), client_address)
                                print(f'{get_time()} SIP to {client_address[0]}:{client_address[1]}: {response}')

    except ConnectionRefusedError:
        print("Error conectando al servidor")


if __name__ == '__main__':
    main()
>>>>>>> origin/master
